# eris

[![forthebadge](http://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/fuck-it-ship-it.svg)](http://forthebadge.com)

a tool to give birth to [dysnomia](https://gitlab.com/dysnomia-framework/dysnomia) instances.

[![forthebadge](http://forthebadge.com/images/badges/built-by-codebabes.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/kinda-sfw.svg)](http://forthebadge.com)


## Installation

for now just download the `eris` executable into your `$PATH`.

more options will be made available when they are.

## Usage

### setting up a new dysnomia instance

    eris install /path/to/the/instance/to/create

this sets up a new instance from a template containing hello world and other examples and a debug config that runs on port 8080 so you don't have to be root.

### setting up dysnomia for an existing (eg. cloned) instance

    eris install

### running dysnomia [tools](https://gitlab.com/dysnomia-framework/dysnomia/tree/master/tools)

    eris the_tool_to_run arguments to said tool

